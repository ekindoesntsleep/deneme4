﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public void ApplyForceUp()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 1000), ForceMode.Force);
    }

    public void ApplyForceLeft()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(-1000, 0, 0), ForceMode.Force);
    }

    public void ApplyForceDown()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -1000), ForceMode.Force);
    }

    public void ApplyForceRight()
    {
        gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(1000, 0, 0), ForceMode.Force);
    }
}
