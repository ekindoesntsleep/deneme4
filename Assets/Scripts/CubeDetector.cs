﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeDetector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        other.GetComponent<Rigidbody>().WakeUp();
        other.GetComponent<MeshRenderer>().enabled = true;
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<Rigidbody>().Sleep();
        other.GetComponent<MeshRenderer>().enabled = false;

    }
}
